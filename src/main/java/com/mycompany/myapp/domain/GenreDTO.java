package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Genre;

public class GenreDTO {
	
	private Integer genreId;
    private String name;
    
    public GenreDTO() 
	{
	}
    
    public GenreDTO(String name) 
	{
		this.name = name;
	}
    
	public GenreDTO(Integer genreId, String name) 
	{
		this.genreId = genreId;
		this.name = name;
	}
	
	public GenreDTO(Genre genre) 
	{
		this.genreId = genre.getGenreId();
		this.name = genre.getName();
	}
	
	public Integer getGenreId() {
		return genreId;
	}
	public void setGenreId(Integer genreId) {
		this.genreId = genreId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
    

}
