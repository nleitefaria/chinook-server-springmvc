package com.mycompany.myapp.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.mycompany.myapp.entity.Invoice;
import com.mycompany.myapp.entity.Invoiceline;

public class InvoiceDTO 
{	
	private Integer invoiceId;
    private CustomerDTO customer;
    private Date invoiceDate;
    private String billingAddress;
    private String billingCity;
    private String billingState;
    private String billingCountry;
    private String billingPostalCode;
    private BigDecimal total;
    private Set<InvoicelineDTO> invoicelines = new HashSet<InvoicelineDTO>(0);
    
    public InvoiceDTO()
    {	
	}
    
	public InvoiceDTO(Integer invoiceId, CustomerDTO customer, Date invoiceDate, String billingAddress, String billingCity, String billingState, String billingCountry, String billingPostalCode, BigDecimal total, Set<InvoicelineDTO> invoicelines) 
	{	
		this.invoiceId = invoiceId;
		this.customer = customer;
		this.invoiceDate = invoiceDate;
		this.billingAddress = billingAddress;
		this.billingCity = billingCity;
		this.billingState = billingState;
		this.billingCountry = billingCountry;
		this.billingPostalCode = billingPostalCode;
		this.total = total;
		this.invoicelines = invoicelines;
	}
	
	public InvoiceDTO(Invoice invoice) 
	{	
		this.invoiceId = invoice.getInvoiceId();
		this.customer = new CustomerDTO(invoice.getCustomer());
		this.invoiceDate = invoice.getInvoiceDate();
		this.billingAddress = invoice.getBillingAddress();
		this.billingCity = invoice.getBillingCity();
		this.billingState = invoice.getBillingState();
		this.billingCountry = invoice.getBillingCountry();
		this.billingPostalCode = invoice.getBillingPostalCode();
		this.total = invoice.getTotal();				
		for(Invoiceline invoiceline : invoice.getInvoicelines()) 
		{		   
		    this.invoicelines.add(new InvoicelineDTO(invoiceline));
		}
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Set<InvoicelineDTO> getInvoicelines() {
		return invoicelines;
	}

	public void setInvoicelines(Set<InvoicelineDTO> invoicelines) {
		this.invoicelines = invoicelines;
	}	
}
