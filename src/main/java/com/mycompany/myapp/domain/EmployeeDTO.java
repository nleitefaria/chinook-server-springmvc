package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Employee;

public class EmployeeDTO 
{
	
	private Integer employeeId;
    private String lastName;
    private String firstName;
    private String title;
    private Date birthDate;
    private Date hireDate;
    private String address;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String phone;
    private String fax;
    private String email;
     
	public EmployeeDTO() 
	{		
	}

	public EmployeeDTO(Integer employeeId, String lastName, String firstName, String title, Date birthDate, Date hireDate, String address, String city, String state, String country, String postalCode, String phone, String fax, String email)
	{
		this.employeeId = employeeId;
		this.lastName = lastName;
		this.firstName = firstName;
		this.title = title;
		this.birthDate = birthDate;
		this.hireDate = hireDate;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.phone = phone;
		this.fax = fax;
		this.email = email;
	}
	
	public EmployeeDTO(Employee employee)
	{
		this.employeeId = employee.getEmployeeId();
		this.lastName = employee.getLastName();
		this.firstName = employee.getFirstName();
		this.title = employee.getTitle();
		this.birthDate = employee.getBirthDate();
		this.hireDate = employee.getHireDate();
		this.address = employee.getAddress();
		this.city = employee.getCity();
		this.state = employee.getState();
		this.country = employee.getCountry();
		this.postalCode = employee.getPostalCode();
		this.phone = employee.getPhone();
		this.fax = employee.getFax();
		this.email = employee.getEmail();
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
