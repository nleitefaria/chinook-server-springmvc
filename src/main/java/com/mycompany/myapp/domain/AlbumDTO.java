package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Album;

public class AlbumDTO 
{
	private Integer albumId;
	private String title;
	private Integer artistId;
	private String artistName;
	
	public AlbumDTO()
	{
	}
	
	public AlbumDTO(String title, Integer artistId) 
	{		
		this.title = title;
		this.artistId = artistId;
	}

	public AlbumDTO(Integer albumId, String title, Integer artistId, String artistName) 
	{
		this.albumId = albumId;
		this.title = title;
		this.artistId = artistId;
		this.artistName = artistName;
	}
	
	public AlbumDTO(Album album) 
	{
		this.albumId = album.getAlbumId();
		this.title = album.getTitle();
		this.artistId = album.getArtist().getArtistId();
		this.artistName = album.getArtist().getName();
	}

	public Integer getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
}
