package com.mycompany.myapp.domain;

import java.util.HashSet;
import java.util.Set;

import com.mycompany.myapp.entity.Album;
import com.mycompany.myapp.entity.Artist;

public class ArtistDTO {
	
	private Integer artistId;
    private String name;
    private Set<AlbumDTO> albums = new HashSet<AlbumDTO>(0);
	
    public ArtistDTO() 
    {
	}
    
	public ArtistDTO(Integer artistId, String name, Set<AlbumDTO> albums) 
	{	
		this.artistId = artistId;
		this.name = name;
		this.albums = albums;
	}

	public ArtistDTO(Artist artist) 
	{	
		this.artistId = artist.getArtistId();
		this.name = artist.getName();		
		Set<AlbumDTO> albums = new HashSet<AlbumDTO>(0);		
		for (Album album : artist.getAlbums()) {
		    AlbumDTO albumDTO = new AlbumDTO(album);
		    albums.add(albumDTO);
		}	
		this.albums = albums;
	}
	
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<AlbumDTO> getAlbums() {
		return albums;
	}

	public void setAlbums(Set<AlbumDTO> albums) {
		this.albums = albums;
	}
}
