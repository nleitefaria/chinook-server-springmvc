package com.mycompany.myapp.domain;

import java.math.BigDecimal;

import com.mycompany.myapp.entity.Invoiceline;


public class InvoicelineDTO
{	
	private Integer invoiceLineId;
	private Integer invoiceId;
    private TrackDTO track;
    private BigDecimal unitPrice;
    private int quantity;
    
	public InvoicelineDTO() 
	{	
	}

	public InvoicelineDTO(Integer invoiceLineId, Integer invoiceId, TrackDTO track, BigDecimal unitPrice, int quantity) 
	{
		this.invoiceLineId = invoiceLineId;
		this.invoiceId = invoiceId;
		this.track = track;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}
    
	public InvoicelineDTO(Invoiceline invoiceline) 
	{
		this.invoiceLineId = invoiceline.getInvoiceLineId();
		this.invoiceId = invoiceline.getInvoice().getInvoiceId();
		this.track = new TrackDTO(invoiceline.getTrack());
		this.unitPrice = invoiceline.getUnitPrice();
		this.quantity = invoiceline.getQuantity();
	}

	public Integer getInvoiceLineId() {
		return invoiceLineId;
	}

	public void setInvoiceLineId(Integer invoiceLineId) {
		this.invoiceLineId = invoiceLineId;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public TrackDTO getTrack() {
		return track;
	}

	public void setTrack(TrackDTO track) {
		this.track = track;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
