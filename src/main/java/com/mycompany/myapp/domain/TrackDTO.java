package com.mycompany.myapp.domain;

import java.math.BigDecimal;

import com.mycompany.myapp.entity.Track;

public class TrackDTO
{
	private Integer trackId;
    private AlbumDTO album;
    private GenreDTO genre;
    private MediatypeDTO mediatype;
    private String name;
    private String composer;
    private int milliseconds;
    private Integer bytes;
    private BigDecimal unitPrice;
    
	public TrackDTO() 
	{	
	}

	public TrackDTO(Integer trackId, AlbumDTO album, GenreDTO genre, MediatypeDTO mediatype, String name, String composer, int milliseconds, Integer bytes, BigDecimal unitPrice)
	{		
		this.trackId = trackId;
		this.album = album;
		this.genre = genre;
		this.mediatype = mediatype;
		this.name = name;
		this.composer = composer;
		this.milliseconds = milliseconds;
		this.bytes = bytes;
		this.unitPrice = unitPrice;
	}
	
	public TrackDTO(Track track)
	{		
		this.trackId = track.getTrackId();
		this.album = new AlbumDTO(track.getAlbum());
		this.genre = new GenreDTO(track.getGenre());
		this.mediatype = new MediatypeDTO(track.getMediatype());
		this.name = track.getName();
		this.composer = track.getComposer();
		this.milliseconds = track.getMilliseconds();
		this.bytes = track.getBytes();
		this.unitPrice = track.getUnitPrice();
	}

	public Integer getTrackId() {
		return trackId;
	}

	public void setTrackId(Integer trackId) {
		this.trackId = trackId;
	}

	public AlbumDTO getAlbum() {
		return album;
	}

	public void setAlbum(AlbumDTO album) {
		this.album = album;
	}

	public GenreDTO getGenre() {
		return genre;
	}

	public void setGenre(GenreDTO genre) {
		this.genre = genre;
	}

	public MediatypeDTO getMediatype() {
		return mediatype;
	}

	public void setMediatype(MediatypeDTO mediatype) {
		this.mediatype = mediatype;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public int getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	public Integer getBytes() {
		return bytes;
	}

	public void setBytes(Integer bytes) {
		this.bytes = bytes;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

}
