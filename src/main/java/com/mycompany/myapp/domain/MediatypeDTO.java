package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Mediatype;

public class MediatypeDTO 
{
	private Integer mediaTypeId;
    private String name;
    
	public MediatypeDTO() 
	{	
	}

	public MediatypeDTO(Integer mediaTypeId, String name)
	{
		this.mediaTypeId = mediaTypeId;
		this.name = name;
	}

	public MediatypeDTO(Mediatype mediatype)
	{	
		this.mediaTypeId = mediatype.getMediaTypeId();
		this.name = mediatype.getName();
	}

	public Integer getMediaTypeId() {
		return mediaTypeId;
	}

	public void setMediaTypeId(Integer mediaTypeId) {
		this.mediaTypeId = mediaTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
