package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Playlist;

public class PlaylistDTO {
	
	private Integer playlistId;
    private String name;
    
    public PlaylistDTO() 
    {		
	}
    
	public PlaylistDTO(Integer playlistId, String name)
	{
		this.playlistId = playlistId;
		this.name = name;
	}
	
	public PlaylistDTO(Playlist playlist) 
	{	
		this.playlistId = playlist.getPlaylistId();
		this.name = playlist.getName();
	}

	public Integer getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(Integer playlistId) {
		this.playlistId = playlistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
