package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.EmployeeDTO;
import com.mycompany.myapp.service.EmployeeService;

@Controller
public class EmployeeController 
{
	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	
	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeeDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing employee with id: " + id);
		return new ResponseEntity<EmployeeDTO>(employeeService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public ResponseEntity<List<EmployeeDTO>> findAll() 
	{
		logger.info("Listing all employees");
		return new ResponseEntity<List<EmployeeDTO>>(employeeService.findAll(), HttpStatus.OK);
	}

}
