package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.PlaylistDTO;
import com.mycompany.myapp.service.PlaylistService;

@Controller
public class PlaylistController 
{	
	private static final Logger logger = LoggerFactory.getLogger(PlaylistController.class);
	
	@Autowired
	PlaylistService playlistService;
	
	@RequestMapping(value = "/playlist/{id}", method = RequestMethod.GET)
	public ResponseEntity<PlaylistDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing playlist with id: " + id);
		return new ResponseEntity<PlaylistDTO>(playlistService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/playlist", method = RequestMethod.GET)
	public ResponseEntity<List<PlaylistDTO>> findAll() 
	{
		logger.info("Listing all playlists");
		return new ResponseEntity<List<PlaylistDTO>>(playlistService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/playlist", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody PlaylistDTO playlistDTO)
	{       
        logger.info("Creating playlist"); 	
		try
		{
			if(playlistService.save(playlistDTO) > 0)
			{
				logger.info("Done");
				return new ResponseEntity<Void>(HttpStatus.CREATED);				
			}
			else
			{
				logger.error("An error ocurred while creating the entity");
				return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);				
			}
			
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	@RequestMapping(value = "/playlist/{id}", method = RequestMethod.PUT)
	public ResponseEntity<PlaylistDTO> update(@PathVariable Integer id, @RequestBody PlaylistDTO playlistDTO) 
	{
		logger.info("Updating playlist with id: " + id);		
		try
		{
			PlaylistDTO ret = playlistService.update(id, playlistDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<PlaylistDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<PlaylistDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<PlaylistDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
}
