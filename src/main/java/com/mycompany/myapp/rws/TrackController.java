package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.TrackDTO;
import com.mycompany.myapp.service.TrackService;

@Controller
public class TrackController {
	
	private static final Logger logger = LoggerFactory.getLogger(TrackController.class);
	
	@Autowired
	TrackService trackService;
	
	@RequestMapping(value = "/track/{id}", method = RequestMethod.GET)
	public ResponseEntity<TrackDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing track with id: " + id);
		return new ResponseEntity<TrackDTO>(trackService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/track", method = RequestMethod.GET)
	public ResponseEntity<List<TrackDTO>> findAll() 
	{
		logger.info("Listing all tracks");
		return new ResponseEntity<List<TrackDTO>>(trackService.findAll(), HttpStatus.OK);
	}

}
