package com.mycompany.myapp.rws;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.GenreDTO;
import com.mycompany.myapp.service.GenreService;

@Controller
public class GenreController 
{
	private static final Logger logger = LoggerFactory.getLogger(GenreController.class);
	
	@Autowired
	GenreService genreService;
	
	@RequestMapping(value = "/genre/{id}", method = RequestMethod.GET)
	public ResponseEntity<GenreDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing actor with id: " + id);
		return new ResponseEntity<GenreDTO>(genreService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/genre", method = RequestMethod.GET)
	public ResponseEntity<List<GenreDTO>> findAll() 
	{
		logger.info("Listing all genres");
		return new ResponseEntity<List<GenreDTO>>(genreService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/genre", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody GenreDTO genreDTO)
	{	
		logger.info("Creating genre"); 			
		try
		{
			genreService.save(genreDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/genre/{id}", method = RequestMethod.PUT)
	public ResponseEntity<GenreDTO> update(@PathVariable Integer id, @RequestBody GenreDTO genreDTO) 
	{
		logger.info("Updating genre with id: " + id);		
		try
		{
			GenreDTO ret = genreService.update(id, genreDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<GenreDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<GenreDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<GenreDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/genre/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> delete(@PathVariable Integer id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Integer ret = genreService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Integer>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);					
		}	     
    }

}
