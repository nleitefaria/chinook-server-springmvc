package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.InvoicelineDTO;
import com.mycompany.myapp.service.InvoicelineService;

@Controller
public class InvoicelineController
{	
	private static final Logger logger = LoggerFactory.getLogger(InvoicelineController.class);
	
	@Autowired
	InvoicelineService invoicelineService;
	
	@RequestMapping(value = "/invoiceline/{id}", method = RequestMethod.GET)
	public ResponseEntity<InvoicelineDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing invoiceline with id: " + id);
		return new ResponseEntity<InvoicelineDTO>(invoicelineService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/invoiceline", method = RequestMethod.GET)
	public ResponseEntity<List<InvoicelineDTO>> findAll() 
	{
		logger.info("Listing all invoicelines");
		return new ResponseEntity<List<InvoicelineDTO>>(invoicelineService.findAll(), HttpStatus.OK);
	}
}
