package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.AlbumDTO;
import com.mycompany.myapp.service.AlbumService;

@Controller
public class AlbumController 
{
	private static final Logger logger = LoggerFactory.getLogger(GenreController.class);
	
	@Autowired
	AlbumService albumService;
	
	@RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
	public ResponseEntity<AlbumDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing album with id: " + id);
		return new ResponseEntity<AlbumDTO>(albumService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/album", method = RequestMethod.GET)
	public ResponseEntity<List<AlbumDTO>> findAll() 
	{
		logger.info("Listing all albuns");
		return new ResponseEntity<List<AlbumDTO>>(albumService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/album", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody AlbumDTO albumDTO)
	{       
        logger.info("Creating album"); 	
		try
		{
			if(albumService.save(albumDTO) > 0)
			{
				logger.info("Done");
				return new ResponseEntity<Void>(HttpStatus.CREATED);				
			}
			else
			{
				logger.error("The associated artist does not exist");
				return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);				
			}
			
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }

}
