package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.MediatypeDTO;
import com.mycompany.myapp.domain.PlaylistDTO;
import com.mycompany.myapp.service.MediatypeService;

@Controller
public class MediatypeController 
{
	private static final Logger logger = LoggerFactory.getLogger(MediatypeController.class);
	
	@Autowired
	MediatypeService mediatypeService;
	
	@RequestMapping(value = "/mediatype/{id}", method = RequestMethod.GET)
	public ResponseEntity<MediatypeDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing mediatype with id: " + id);
		return new ResponseEntity<MediatypeDTO>(mediatypeService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mediatype", method = RequestMethod.GET)
	public ResponseEntity<List<MediatypeDTO>> findAll() 
	{
		logger.info("Listing all mediatypes");
		return new ResponseEntity<List<MediatypeDTO>>(mediatypeService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mediatype", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody MediatypeDTO mediatypeDTO)
	{	
		logger.info("Creating mediatype"); 			
		try
		{
			mediatypeService.save(mediatypeDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	
	@RequestMapping(value = "/mediatype/{id}", method = RequestMethod.PUT)
	public ResponseEntity<MediatypeDTO> update(@PathVariable Integer id, @RequestBody MediatypeDTO mediatypeDTO) 
	{
		logger.info("Updating mediatype with id: " + id);		
		try
		{
			MediatypeDTO ret = mediatypeService.update(id, mediatypeDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<MediatypeDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<MediatypeDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<MediatypeDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	
}
