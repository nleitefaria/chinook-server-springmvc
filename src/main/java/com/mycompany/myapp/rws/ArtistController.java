package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.ArtistDTO;
import com.mycompany.myapp.service.ArtistService;

@Controller
public class ArtistController 
{
	private static final Logger logger = LoggerFactory.getLogger(GenreController.class);
	
	@Autowired
	ArtistService artistService;
	
	@RequestMapping(value = "/artist/{id}", method = RequestMethod.GET)
	public ResponseEntity<ArtistDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing artist with id: " + id);
		return new ResponseEntity<ArtistDTO>(artistService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/artist", method = RequestMethod.GET)
	public ResponseEntity<List<ArtistDTO>> findAll() 
	{
		logger.info("Listing all artists");
		return new ResponseEntity<List<ArtistDTO>>(artistService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/artist", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody ArtistDTO artistDTO)
	{	
		logger.info("Creating artist"); 			
		try
		{
			artistService.save(artistDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/artist/{id}", method = RequestMethod.PUT)
	public ResponseEntity<ArtistDTO> update(@PathVariable Integer id, @RequestBody ArtistDTO artistDTO) 
	{
		logger.info("Updating artist with id: " + id);		
		try
		{
			ArtistDTO ret = artistService.update(id, artistDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<ArtistDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<ArtistDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<ArtistDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/artist/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> delete(@PathVariable Integer id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Integer ret = artistService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Integer>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);					
		}	     
    }
	
}
