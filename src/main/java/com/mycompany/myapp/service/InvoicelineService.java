package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.InvoicelineDTO;

public interface InvoicelineService {
	
	InvoicelineDTO findOne(Integer id);
	List<InvoicelineDTO> findAll();

}
