package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.MediatypeDTO;

public interface MediatypeService 
{
	MediatypeDTO findOne(Integer id);
	List<MediatypeDTO> findAll();
	int save(MediatypeDTO mediatypeDTO) throws javax.persistence.RollbackException;
	MediatypeDTO update(Integer id, MediatypeDTO mediatypeDTO) throws javax.persistence.RollbackException;
}
