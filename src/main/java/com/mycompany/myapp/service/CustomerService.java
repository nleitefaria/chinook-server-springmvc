package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CustomerDTO;

public interface CustomerService 
{
	CustomerDTO findOne(Integer id);
	List<CustomerDTO> findAll();
	int save(CustomerDTO customerDTO) throws javax.persistence.RollbackException;

}
