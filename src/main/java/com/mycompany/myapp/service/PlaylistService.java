package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PlaylistDTO;

public interface PlaylistService 
{
	PlaylistDTO findOne(Integer id);
	List<PlaylistDTO> findAll();
	int save(PlaylistDTO playlistDTO) throws javax.persistence.RollbackException;
	PlaylistDTO update(Integer id, PlaylistDTO playlistDTO) throws javax.persistence.RollbackException;
}
