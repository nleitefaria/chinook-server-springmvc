package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.EmployeeDTO;

public interface EmployeeService
{	
	EmployeeDTO findOne(Integer id);
	List<EmployeeDTO> findAll();
}
