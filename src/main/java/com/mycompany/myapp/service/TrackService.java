package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.TrackDTO;

public interface TrackService {
	
	TrackDTO findOne(Integer id);
	List<TrackDTO> findAll();

}
