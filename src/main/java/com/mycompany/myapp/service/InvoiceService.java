package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.InvoiceDTO;

public interface InvoiceService 
{	
	InvoiceDTO findOne(Integer id);
	List<InvoiceDTO> findAll();
}
