package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ArtistDTO;

public interface ArtistService 
{
	ArtistDTO findOne(Integer id);
	List<ArtistDTO> findAll();
	int save(ArtistDTO artistDTO) throws javax.persistence.RollbackException;
	ArtistDTO update(Integer id, ArtistDTO artistDTO) throws javax.persistence.RollbackException;
	Integer delete(Integer id) throws javax.persistence.RollbackException;
}
