package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.InvoicelineDTO;
import com.mycompany.myapp.entity.Invoiceline;
import com.mycompany.myapp.repository.InvoicelineRepository;
import com.mycompany.myapp.service.InvoicelineService;

@Service
public class InvoicelineServiceImpl implements InvoicelineService {
	
	@Autowired
	InvoicelineRepository invoicelineRepository;
	
	@Transactional
	public InvoicelineDTO findOne(Integer id) 
	{
		Invoiceline invoiceline = invoicelineRepository.findOne(id);
		return new InvoicelineDTO(invoiceline);
	}
	
	@Transactional
	public List<InvoicelineDTO> findAll()
	{
		List<InvoicelineDTO> ret = new ArrayList<InvoicelineDTO>();
		
		for(Invoiceline invoiceline : invoicelineRepository.findAll())
		{
			InvoicelineDTO invoicelineDTO = new InvoicelineDTO(invoiceline);
			ret.add(invoicelineDTO);
		}
		
		return ret;
	}

}
