package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.myapp.domain.ArtistDTO;
import com.mycompany.myapp.entity.Artist;
import com.mycompany.myapp.repository.ArtistRepository;
import com.mycompany.myapp.service.ArtistService;

@Service
public class ArtistServiceImpl implements ArtistService 
{
	@Autowired
	ArtistRepository artistRepository;
	
	@Transactional
	public ArtistDTO findOne(Integer id)
	{
		Artist artist = artistRepository.findOne(id);
		return new ArtistDTO(artist);
	}
	
	@Transactional
	public List<ArtistDTO> findAll()
	{
		List<ArtistDTO> ret = new ArrayList<ArtistDTO>();
		
		for(Artist artist : artistRepository.findAll())
		{
			ArtistDTO artistDTO = new ArtistDTO(artist);
			ret.add(artistDTO);
		}		
		return ret;
	}
	
	@Transactional
	public int save(ArtistDTO artistDTO) throws javax.persistence.RollbackException   
	{
		Artist artist = new Artist();
		artist.setName(artistDTO.getName());
		artistRepository.save(artist);
		return 1;			
	}
	
	@Transactional
	public ArtistDTO update(Integer id, ArtistDTO artistDTO) throws javax.persistence.RollbackException   
	{		
		Artist artist = artistRepository.findOne(id);
			
		if(artist != null)
		{
			artist.setName(artistDTO.getName());		
			ArtistDTO ret = new ArtistDTO(artist);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Integer delete(Integer id) throws javax.persistence.RollbackException   
	{		
		Artist artist = artistRepository.findOne(id);
		
		if(artist != null)
		{
			artistRepository.delete(artist);
			return id;
		}
		else
		{
			return null;		
		}			
	}
}