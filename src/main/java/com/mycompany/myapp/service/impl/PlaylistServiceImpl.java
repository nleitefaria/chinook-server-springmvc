package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.PlaylistDTO;
import com.mycompany.myapp.entity.Playlist;
import com.mycompany.myapp.repository.PlaylistRepository;
import com.mycompany.myapp.service.PlaylistService;

@Service
public class PlaylistServiceImpl implements PlaylistService
{	
	@Autowired
	PlaylistRepository playlistRepository;
	
	@Transactional
	public PlaylistDTO findOne(Integer id) {
		Playlist playlist = playlistRepository.findOne(id);
		return new PlaylistDTO(playlist);
	}
	
	@Transactional
	public List<PlaylistDTO> findAll()
	{
		List<PlaylistDTO> ret = new ArrayList<PlaylistDTO>();
		
		for(Playlist playlist : playlistRepository.findAll())
		{
			PlaylistDTO playlistDTO = new PlaylistDTO(playlist);
			ret.add(playlistDTO);
		}
		
		return ret;
	}
	
	@Transactional
	public int save(PlaylistDTO playlistDTO) throws javax.persistence.RollbackException   
	{		
		Playlist playlist = new Playlist();
		playlist.setName(playlistDTO.getName());
		playlistRepository.save(playlist);
		return 1;				
	}
	
	@Transactional
	public PlaylistDTO update(Integer id, PlaylistDTO playlistDTO) throws javax.persistence.RollbackException   
	{		
		Playlist playlist = playlistRepository.findOne(id);
			
		if(playlist != null)
		{
			playlist.setName(playlistDTO.getName());		
			PlaylistDTO ret = new PlaylistDTO(playlist);
			return ret;
		}
		else
		{
				return null;		
		}			
	}

}
