package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EmployeeDTO;
import com.mycompany.myapp.entity.Employee;
import com.mycompany.myapp.repository.EmployeeRepository;
import com.mycompany.myapp.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Transactional
	public EmployeeDTO findOne(Integer id) {
		Employee employee = employeeRepository.findOne(id);
		return new EmployeeDTO(employee);
	}
	
	@Transactional
	public List<EmployeeDTO> findAll()
	{
		List<EmployeeDTO> ret = new ArrayList<EmployeeDTO>();
		
		for(Employee employee : employeeRepository.findAll())
		{
			EmployeeDTO employeeDTO = new EmployeeDTO(employee);
			ret.add(employeeDTO);
		}
		
		return ret;
	}
}
