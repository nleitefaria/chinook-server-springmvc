package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.InvoiceDTO;
import com.mycompany.myapp.entity.Invoice;
import com.mycompany.myapp.repository.InvoiceRepository;
import com.mycompany.myapp.service.InvoiceService;

@Service
public class InvoiceServiceImpl implements InvoiceService{
	
	@Autowired
	InvoiceRepository invoiceRepository;
	
	@Transactional
	public InvoiceDTO findOne(Integer id) 
	{
		Invoice invoice = invoiceRepository.findOne(id);
		return new InvoiceDTO(invoice);
	}
	
	@Transactional
	public List<InvoiceDTO> findAll()
	{
		List<InvoiceDTO> ret = new ArrayList<InvoiceDTO>();
		
		for(Invoice invoice : invoiceRepository.findAll())
		{
			InvoiceDTO invoiceDTO = new InvoiceDTO(invoice);
			ret.add(invoiceDTO);
		}
		
		return ret;
	}

}
