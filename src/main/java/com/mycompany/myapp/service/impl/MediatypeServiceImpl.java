package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.MediatypeDTO;
import com.mycompany.myapp.entity.Mediatype;
import com.mycompany.myapp.repository.MediatypeRepository;
import com.mycompany.myapp.service.MediatypeService;

@Service
public class MediatypeServiceImpl implements MediatypeService {
	
	@Autowired
	MediatypeRepository mediatypeRepository;
	
	@Transactional
	public MediatypeDTO findOne(Integer id) {
		Mediatype mediatype = mediatypeRepository.findOne(id);
		return new MediatypeDTO(mediatype);
	}
	
	@Transactional
	public List<MediatypeDTO> findAll()
	{
		List<MediatypeDTO> ret = new ArrayList<MediatypeDTO>();
		
		for(Mediatype mediatype : mediatypeRepository.findAll())
		{
			MediatypeDTO mediatypeDTO = new MediatypeDTO(mediatype);
			ret.add(mediatypeDTO);
		}
		
		return ret;
	}
	
	@Transactional
	public int save(MediatypeDTO mediatypeDTO) throws javax.persistence.RollbackException   
	{
		Mediatype mediatype = new Mediatype();
		mediatype.setName(mediatypeDTO.getName());
		mediatypeRepository.save(mediatype);
		return 1;			
	}
	
	@Transactional
	public MediatypeDTO update(Integer id, MediatypeDTO mediatypeDTO) throws javax.persistence.RollbackException   
	{		
		Mediatype mediatype = mediatypeRepository.findOne(id);
			
		if(mediatype != null)
		{
			mediatype.setName(mediatypeDTO.getName());		
			MediatypeDTO ret = new MediatypeDTO(mediatype);
			return ret;
		}
		else
		{
				return null;		
		}			
	}

}
