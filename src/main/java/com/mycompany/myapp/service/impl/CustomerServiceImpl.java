package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.ArtistDTO;
import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.entity.Artist;
import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Transactional
	public CustomerDTO findOne(Integer id) {
		Customer customer = customerRepository.findOne(id);
		return new CustomerDTO(customer);
	}
	
	@Transactional
	public List<CustomerDTO> findAll()
	{
		List<CustomerDTO> ret = new ArrayList<CustomerDTO>();
		
		for(Customer customer : customerRepository.findAll())
		{
			CustomerDTO customerDTO = new CustomerDTO(customer);
			ret.add(customerDTO);
		}
		
		return ret;
	}
	
	@Transactional
	public int save(CustomerDTO customerDTO) throws javax.persistence.RollbackException   
	{
		Customer customer = new Customer(customerDTO.getFirstName(), customerDTO.getLastName(), customerDTO.getEmail());	
		customerRepository.save(customer);
		return 1;			
	}

}
