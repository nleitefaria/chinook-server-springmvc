package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.GenreDTO;
import com.mycompany.myapp.entity.Genre;
import com.mycompany.myapp.repository.GenreRepository;
import com.mycompany.myapp.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService 
{
	@Autowired
	GenreRepository genreRepository;
	
	@Transactional
	public GenreDTO findOne(Integer id) {
		Genre genre = genreRepository.findOne(id);
		return new GenreDTO(genre);
	}
	
	@Transactional
	public List<GenreDTO> findAll()
	{
		List<GenreDTO> ret = new ArrayList<GenreDTO>();
		
		for(Genre genre : genreRepository.findAll())
		{
			GenreDTO genreDTO = new GenreDTO(genre);
			ret.add(genreDTO);
		}
		
		return ret;
	}
	
	@Transactional
	public int save(GenreDTO genreDTO) throws javax.persistence.RollbackException   
	{
		Genre genre = new Genre();
		genre.setName(genreDTO.getName());
		genreRepository.save(genre);
		return 1;			
	}
	
	@Transactional
	public GenreDTO update(Integer id, GenreDTO genreDTO) throws javax.persistence.RollbackException   
	{		
		Genre genre = genreRepository.findOne(id);
			
		if(genre != null)
		{
			genre.setName(genreDTO.getName());		
			GenreDTO ret = new GenreDTO(genre);
			return ret;
		}
		else
		{
				return null;		
		}			
	}
	
	@Transactional
	public Integer delete(Integer id) throws javax.persistence.RollbackException   
	{		
		Genre genre = genreRepository.findOne(id);
		
		if(genre != null)
		{
			genreRepository.delete(genre);
			return id;
		}
		else
		{
			return null;		
		}			
	}
}
