package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AlbumDTO;
import com.mycompany.myapp.entity.Album;
import com.mycompany.myapp.entity.Artist;
import com.mycompany.myapp.repository.AlbumRepository;
import com.mycompany.myapp.repository.ArtistRepository;
import com.mycompany.myapp.service.AlbumService;

@Service
public class AlbumServiceImpl implements AlbumService 
{	
	@Autowired
	AlbumRepository albumRepository;
	
	@Autowired
	ArtistRepository artistRepository;
	
	@Transactional
	public AlbumDTO findOne(Integer id) {
		Album album = albumRepository.findOne(id);
		return new AlbumDTO(album);
	}
	
	@Transactional
	public List<AlbumDTO> findAll()
	{
		List<AlbumDTO> ret = new ArrayList<AlbumDTO>();
		
		for(Album album : albumRepository.findAll())
		{
			AlbumDTO albumDTO = new AlbumDTO(album);
			ret.add(albumDTO);
		}
		
		return ret;
	}
	
	@Transactional
	public int save(AlbumDTO albumDTO) throws javax.persistence.RollbackException   
	{
		Artist artist = artistRepository.findOne(albumDTO.getArtistId());
		
		if(artist == null)
		{
			return -1;			
		}
		else
		{
			Album album = new Album(artist, albumDTO.getTitle());
			albumRepository.save(album);
			return 1;
		}			
	}

}
