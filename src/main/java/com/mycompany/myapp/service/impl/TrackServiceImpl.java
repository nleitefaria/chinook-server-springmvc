package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.TrackDTO;
import com.mycompany.myapp.entity.Track;
import com.mycompany.myapp.repository.TrackRepository;
import com.mycompany.myapp.service.TrackService;

@Service
public class TrackServiceImpl implements TrackService 
{	
	@Autowired
	TrackRepository trackRepository;
	
	@Transactional
	public TrackDTO findOne(Integer id) 
	{
		Track track = trackRepository.findOne(id);
		return new TrackDTO(track);
	}
	
	@Transactional
	public List<TrackDTO> findAll()
	{
		List<TrackDTO> ret = new ArrayList<TrackDTO>();
		
		for(Track track : trackRepository.findAll())
		{
			TrackDTO trackDTO = new TrackDTO(track);
			ret.add(trackDTO);
		}
		
		return ret;
	}


}
