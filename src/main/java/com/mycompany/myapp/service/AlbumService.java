package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.AlbumDTO;

public interface AlbumService 
{	
	AlbumDTO findOne(Integer id);
	List<AlbumDTO> findAll();
	int save(AlbumDTO albumDTO) throws javax.persistence.RollbackException;
}
