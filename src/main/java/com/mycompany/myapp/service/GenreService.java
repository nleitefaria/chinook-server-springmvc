package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.GenreDTO;

public interface GenreService 
{
	GenreDTO findOne(Integer id);
	List<GenreDTO> findAll();
	int save(GenreDTO genreDTO) throws javax.persistence.RollbackException ;
	GenreDTO update(Integer id, GenreDTO genreDTO) throws javax.persistence.RollbackException;
	Integer delete(Integer id) throws javax.persistence.RollbackException;
}
