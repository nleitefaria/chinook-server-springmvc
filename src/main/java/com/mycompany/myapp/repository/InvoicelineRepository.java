package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Invoiceline;

public interface InvoicelineRepository extends JpaRepository<Invoiceline, Integer>, JpaSpecificationExecutor<Invoiceline> {

}
