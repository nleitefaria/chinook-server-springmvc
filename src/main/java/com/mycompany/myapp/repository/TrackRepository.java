package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Track;

public interface TrackRepository  extends JpaRepository<Track, Integer>, JpaSpecificationExecutor<Track> {

}
