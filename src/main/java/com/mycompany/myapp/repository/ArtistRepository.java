package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Artist;

public interface ArtistRepository  extends JpaRepository<Artist, Integer>, JpaSpecificationExecutor<Artist> {


}
