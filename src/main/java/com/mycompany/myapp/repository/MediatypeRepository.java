package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Mediatype;

public interface MediatypeRepository extends JpaRepository<Mediatype, Integer>, JpaSpecificationExecutor<Mediatype> {

}
